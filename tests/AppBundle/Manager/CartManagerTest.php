<?php

namespace tests\AppBundle\Manager;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductRepository;
use AppBundle\Manager\CartManager;
use Doctrine\ORM\EntityManager;
use Money\Currency;
use Money\Money;
use Symfony\Component\HttpFoundation\Session\Session;

class CartManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testAddProduct()
    {
        $product = \Mockery::mock(Product::class);
        $product->shouldReceive('getId')->andReturn(1);

        $session = \Mockery::mock(Session::class);
        $session->shouldReceive('getId')->andReturn('s1');
        $session->shouldReceive('get')->andReturn([]);
        $session->shouldReceive('set');

        $productRepo = \Mockery::mock(ProductRepository::class);
        $productRepo->shouldReceive('findBy')
            ->andReturn([$product]);
        $em = \Mockery::mock(EntityManager::class);
        $em->shouldReceive('getRepository')
            ->andReturn($productRepo);

        $cartManager = new CartManager($session, $em);

        $this->assertTrue($cartManager->addProduct($product));
    }

    public function testAddDuplicateProduct()
    {
        $product = \Mockery::mock(Product::class);
        $product->shouldReceive('getId')->andReturn(1);

        $session = \Mockery::mock(Session::class);
        $session->shouldReceive('getId')->andReturn('s1');
        $session->shouldReceive('get')->twice()->andReturnValues([[], [1]]);
        $session->shouldReceive('set');

        $productRepo = \Mockery::mock(ProductRepository::class);
        $productRepo->shouldReceive('findBy')
            ->andReturn([$product]);
        $em = \Mockery::mock(EntityManager::class);
        $em->shouldReceive('getRepository')
            ->andReturn($productRepo);

        $cartManager = new CartManager($session, $em);

        $this->assertTrue($cartManager->addProduct($product));
        $this->assertFalse($cartManager->addProduct($product));
    }

    public function testGetTotal()
    {
        $product1 = \Mockery::mock(Product::class);
        $product1->shouldReceive('getId')->andReturn(1);
        $product1->shouldReceive('getPrice')->andReturn(new Money(100, new Currency('EUR')));
        $product2 = \Mockery::mock(Product::class);
        $product2->shouldReceive('getId')->andReturn(2);
        $product2->shouldReceive('getPrice')->andReturn(new Money(200, new Currency('EUR')));

        $session = \Mockery::mock(Session::class);
        $session->shouldReceive('getId')->andReturn('s1');
        $session->shouldReceive('get')->twice()->andReturnValues([[1], [1, 2]]);
        $session->shouldReceive('set');

        $productRepo = \Mockery::mock(ProductRepository::class);
        $productRepo->shouldReceive('findBy')
            ->andReturnValues([[$product1], [$product1, $product2]]);
        $em = \Mockery::mock(EntityManager::class);
        $em->shouldReceive('getRepository')
            ->andReturn($productRepo);

        $cartManager = new CartManager($session, $em);
        $cartManager->addProduct($product1);
        $cartManager->addProduct($product2);
        $cartManager->clearBuffer();

        $this->assertEquals(300, $cartManager->getTotal()->getAmount());
    }
}
