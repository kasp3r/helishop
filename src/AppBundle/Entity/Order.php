<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="OrderRepository")
 * @ORM\Table(name="`order`")
 */
class Order
{
    use TimestampableEntity;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="Product")
     */
    private $product;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @Gedmo\Blameable(on="create")
     */
    private $orderedBy;

    /**
     * @return User
     */
    public function getOrderedBy(): User
    {
        return $this->orderedBy;
    }

    /**
     * @param User $orderedBy
     * @return Order
     */
    public function setOrderedBy(User $orderedBy): Order
    {
        $this->orderedBy = $orderedBy;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return Order
     */
    public function setProduct(Product $product): Order
    {
        $this->product = $product;

        return $this;
    }
}