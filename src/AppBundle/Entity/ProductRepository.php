<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ProductRepository
 * @package AppBundle\Entity
 */
class ProductRepository extends EntityRepository
{
    /**
     * @param Category|null $category
     * @return QueryBuilder
     */
    public function getListBuilder(Category $category = null)
    {
        $qb = $this->createQueryBuilder('p');
        if ($category instanceof Category) {
            $qb
                ->innerJoin('p.categories', 'c')
                ->where('c = :category')
                ->setParameter('category', $category);
        }

        return $qb;
    }
}
