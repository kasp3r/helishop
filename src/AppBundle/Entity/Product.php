<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Money\Currency;
use Money\Money;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="ProductRepository")
 * @ORM\Table(name="product")
 */
class Product
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Money
     *
     * @ORM\Column(type="money")
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\Length(min="10")
     */
    private $description;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ProductPicture", mappedBy="product", cascade={"persist"})
     */
    private $pictures;

    /**
     * @var array
     * @Assert\Count(min="1")
     */
    private $uploadedFiles;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"description"})
     * @ORM\Column(type="string", length=64)
     */
    private $slug;

    /**
     * @var User
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Category")
     */
    private $categories;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pictures = new ArrayCollection();
        $this->uploadedFiles = [];
        $this->categories = new ArrayCollection();
        $this->description = '';
        $this->price = new Money(0, new Currency('EUR'));
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     * @return Product
     */
    public function setCreatedBy(User $createdBy): Product
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get price
     *
     * @return Money
     */
    public function getPrice(): Money
    {
        return $this->price;
    }

    /**
     * Set price
     *
     * @param Money $price
     *
     * @return Product
     */
    public function setPrice(Money $price): Product
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription(string $description): Product
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Product
     */
    public function setSlug(string $slug): Product
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Add picture
     *
     * @param ProductPicture $picture
     *
     * @return Product
     */
    public function addPicture(ProductPicture $picture): Product
    {
        $this->pictures[] = $picture;

        return $this;
    }

    /**
     * Remove picture
     *
     * @param ProductPicture $picture
     */
    public function removePicture(ProductPicture $picture)
    {
        $this->pictures->removeElement($picture);
    }

    /**
     * Get pictures
     *
     * @return Collection
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    /**
     * @return UploadedFile[]
     */
    public function getUploadedFiles(): array
    {
        return $this->uploadedFiles;
    }

    /**
     * @param UploadedFile[] $uploadedFiles
     * @return Product
     */
    public function setUploadedFiles(array $uploadedFiles): Product
    {
        $this->uploadedFiles = $uploadedFiles;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Collection $categories
     * @return Product
     */
    public function setCategories(Collection $categories): Product
    {
        $this->categories = $categories;

        return $this;
    }
}
