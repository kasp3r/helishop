<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

/**
 * User
 *
 * @Table(name="sessions")
 * @Entity
 */
class Session
{
    /**
     * @var int
     *
     * @Column(name="sess_id", type="string", length=128)
     * @Id
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="sess_data", type="string", length=4000)
     */
    private $data;

    /**
     * @var int
     *
     * @Column(name="sess_time", type="integer")
     */
    private $time;

    /**
     * @var int
     *
     * @Column(name="sess_lifetime", type="integer")
     */
    private $lifetime;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param int $id
     *
     * @return Session
     */
    public function setId(int $id): Session
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return Session
     */
    public function setData(string $data): Session
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get time
     *
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * Set time
     *
     * @param int $time
     *
     * @return Session
     */
    public function setTime(int $time): Session
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get lifetime
     *
     * @return int
     */
    public function getLifetime(): int
    {
        return $this->lifetime;
    }

    /**
     * Set lifetime
     *
     * @param int $lifetime
     *
     * @return Session
     */
    public function setLifetime(int $lifetime): Session
    {
        $this->lifetime = $lifetime;

        return $this;
    }
}
