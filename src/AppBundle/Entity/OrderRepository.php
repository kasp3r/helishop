<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class OrderRepository
 * @package AppBundle\Entity
 */
class OrderRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return array
     */
    public function getOrderRequests(User $user): array
    {
        $qb = $this->createQueryBuilder('o')
            ->innerJoin('o.product', 'p')
            ->where('p.createdBy = :user')
            ->setParameters(['user' => $user]);

        return $qb->getQuery()->getResult();
    }
}
