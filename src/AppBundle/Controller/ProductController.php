<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProductController
 * @package AppBundle\Controller
 */
class ProductController extends Controller
{
    const PRODUCTS_PER_PAGE = 10;

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Product::class);
        $categoryRepository = $em->getRepository(Category::class);

        $categoryId = (int)$request->get('category');
        $category = null;
        if ($categoryId) {
            $category = $categoryRepository->find($categoryId);
        }
        $listBuilder = $repository->getListBuilder($category);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $listBuilder,
            $request->query->getInt('page', 1),
            self::PRODUCTS_PER_PAGE
        );

        return $this->render(
            '@App/product/index.html.twig',
            ['pagination' => $pagination, 'categories' => $categoryRepository->findAll()]
        );
    }

    /**
     * @Route(name="product_view", path="product/{product}-{slug}", requirements={"product": "\d+"})
     * @param Product $product
     * @return Response
     */
    public function viewAction(Product $product): Response
    {
        return $this->render('@App/product/view.html.twig', ['product' => $product]);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route(path="product/add", name="product_add")
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request): Response
    {
        $form = $this->createForm(ProductType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash('success', 'Your product is saved');

            return $this->redirectToRoute(
                'product_view',
                ['product' => $product->getId(), 'slug' => $product->getSlug()]
            );
        }

        return $this->render('@App/product/add.html.twig', ['form' => $form->createView()]);
    }
}