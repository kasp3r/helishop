<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CartController
 * @package AppBundle\Controller
 */
class CartController extends Controller
{
    /**
     * @Route(name="cart_widget", path="cart/widget")
     * @return Response
     */
    public function widgetAction(): Response
    {
        $cart = $this->get('app.cart_manager');
        $products = $cart->getProducts();

        return $this->render('@App/cart/widget.html.twig', ['products' => $products, 'total' => $cart->getTotal()]);
    }

    /**
     * @Route(name="cart_add", path="cart/add", methods={"post"}, requirements={"product": "\d+"})
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request): Response
    {
        $productId = $request->get('product');
        $repo = $this->getDoctrine()->getRepository(Product::class);
        $product = $repo->find($productId);
        if (!$product instanceof Product) {
            return new Response('Invalid product');
        }

        $cart = $this->get('app.cart_manager');
        $isProductAdded = $cart->addProduct($product);

        return new Response($isProductAdded ? 'Product added to cart' : 'Product is already in your cart');
    }
}
