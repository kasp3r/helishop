<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Order;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OrderController
 * @package AppBundle\Controller
 */
class OrderController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route(name="order_checkout", path="order/checkout")
     * @return RedirectResponse
     */
    public function checkoutAction(): RedirectResponse
    {
        $orderManager = $this->get('app.order_manager');
        $orderManager->checkout();

        $this->addFlash('success', 'Your orders sent to a seller');

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route(name="order_requests", path="order/requests")
     * @return Response
     */
    public function orderRequestsAction(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Order::class);
        $orders = $repo->getOrderRequests($this->getUser());

        return $this->render('@App/order/requests.html.twig', ['orders' => $orders]);
    }
}