<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Order;
use Doctrine\ORM\EntityManager;

/**
 * Class OrderManager
 * @package AppBundle\Manager
 */
class OrderManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var CartManager
     */
    private $cartManager;

    /**
     * OrderManager constructor.
     * @param EntityManager $entityManager
     * @param CartManager $cartManager
     */
    public function __construct(EntityManager $entityManager, CartManager $cartManager)
    {
        $this->entityManager = $entityManager;
        $this->cartManager = $cartManager;
    }

    /**
     * Checkout cart products
     */
    public function checkout()
    {
        $products = $this->cartManager->getProducts();
        foreach ($products as $product) {
            $order = (new Order())->setProduct($product);
            $this->entityManager->persist($order);
        }
        $this->entityManager->flush();

        $this->cartManager->clear();
    }
}