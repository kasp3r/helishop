<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Product;
use Doctrine\ORM\EntityManager;
use Money\Currency;
use Money\Money;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class CartManager
 * @package AppBundle\Manager
 */
class CartManager
{
    /**
     * @var Session
     */
    private $session;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var array
     */
    private $productsBuffer = [];

    /**
     * CartManager constructor.
     * @param Session $session
     * @param EntityManager $entityManager
     */
    public function __construct(Session $session, EntityManager $entityManager)
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function addProduct(Product $product): bool
    {
        $cartProducts = $this->getProducts();
        foreach ($cartProducts as $cartProduct) {
            if ($cartProduct->getId() === $product->getId()) {
                return false;
            }
        }

        $cartProducts[] = $product;
        $products = array_map(
            function ($product) {
                return $product->getId();
            },
            $cartProducts
        );

        $this->session->set($this->getCartKey(), $products);

        return true;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        if (count($this->productsBuffer) > 0) {
            return $this->productsBuffer;
        }

        $products = $this->session->get($this->getCartKey(), []);
        if (empty($products)) {
            return [];
        }

        $repository = $this->entityManager->getRepository(Product::class);
        $products = $repository->findBy(['id' => $products]);
        $this->productsBuffer = $products;

        return $products;
    }

    /**
     * @return string
     */
    private function getCartKey(): string
    {
        return sprintf('cart_%d', $this->session->getId());
    }

    /**
     * @return CartManager
     */
    public function clear(): CartManager
    {
        $this->session->set($this->getCartKey(), []);
        $this->clearBuffer();

        return $this;
    }

    /**
     * @return CartManager
     */
    public function clearBuffer(): CartManager
    {
        $this->productsBuffer = [];

        return $this;
    }

    /**
     * @return Money
     */
    public function getTotal(): Money
    {
        $products = $this->getProducts();
        $total = new Money(0, new Currency('EUR'));
        foreach ($products as $product) {
            $total = $total->add($product->getPrice());
        }

        return $total;
    }
}