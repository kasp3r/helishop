<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductPicture;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use League\Flysystem\Filesystem;

/**
 * Class ProductPictureUploadListener
 * @package AppBundle\EventListener
 */
class ProductPictureUploadListener
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * ProductPictureUploadListener constructor.
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Product) {
            return;
        }

        $this->uploadFiles($entity, $args->getEntityManager());
    }

    /**
     * @param Product $entity
     * @param EntityManager $entityManager
     */
    private function uploadFiles(Product $entity, EntityManager $entityManager)
    {
        $files = $entity->getUploadedFiles();
        foreach ($files as $file) {
            if (!$file->isValid()) {
                return;
            }

            $newName = sprintf(
                '%d%s%d.%s',
                time(),
                md5($file->getClientOriginalName()),
                rand(0, 99999),
                $file->getClientOriginalExtension()
            );

            $stream = fopen($file->getRealPath(), 'r+');
            $this->filesystem->writeStream($newName, $stream);
            fclose($stream);

            $picture = new ProductPicture();
            $picture
                ->setName($newName)
                ->setPath('')
                ->setSize($file->getSize())
                ->setProduct($entity);
            $entityManager->persist($picture);
        }
    }
}