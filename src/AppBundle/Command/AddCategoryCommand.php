<?php

namespace AppBundle\Command;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AddCategoryCommand
 * @package AppBundle\Command
 */
class AddCategoryCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('app:category:add')
            ->setDescription('Creates a new category.')
            ->setHelp('This command allows you to add category...')
            ->addArgument('title', InputArgument::REQUIRED, 'Category title.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $title = $input->getArgument('title');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $repo = $em->getRepository(Category::class);
        $category = $repo->findOneByTitle($title);
        if ($category instanceof Category) {
            $output->writeln('Category with this title already exists');

            return;
        }

        $category = (new Category())->setTitle($title);
        $em->persist($category);
        $em->flush();

        $output->writeln('Category added');
    }
}