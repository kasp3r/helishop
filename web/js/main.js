$(function (){
    $('.buy-now').click(function (e) {
        e.preventDefault();

        $.post('/cart/add', {'product': $(this).data('product-id')}, function (data) {
            alert(data);

            $.get('/cart/widget', function (data) {
                $('.cart-widget').html(data);
            });
        });
    });
});