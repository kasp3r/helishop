helishop
========

Helis technical task to create eshop like vinted.
### Task
Create a small web application that must implement:

* User system
    * User can register
    * User can login
    * Logged user can change his password
    * User can logout
* User Items
    * Logged user can add items they want to sell they want to sell with price, description and picture(s)
    * Logged user can assign predefined category(ies) for the item
    add items to they shoping cart
* User cart
    * Display list of items
    * Total price
    * When user press Buy button in Cart:
        * If user is not logged in - ask to log in or create account and after that proceed buy process
        create buy request(s) for user(s), who sells the items, with items that user, who pressed the buy button, is interested buying
* In frontend list of all users selling items
    * Posibility to order by latest items
    * Posibility to order by price (highest, lowest)
    * Filter by predefined categories
    * When clicked on item a page with more detailed information is opened


## Requirements
* Mysql
* PHP >=7

## Installation
Install composer

Run `composer install`

Edit `app/config/parameters.yml`

`php bin/console doctrine:database:create`

`php bin/console doctrine:schema:update --force`

`php bin/console assetic:dump`

`php bin/console server:run`

Open `http://127.0.0.1:8000` in browser

Add categories using console command `php bin/console app:category:add <title>` 

## Tests

Run tests with command `php vendor/bin/simple-phpunit`
